const headerElement = document.querySelector('.sc-header--scrollable')
const breakingPointElement = document.querySelector('.banner');

const options = {
    rootMargin: "-110px 0px 0px 0px",
}

// @ts-ignore
const observer = new IntersectionObserver(function (entries, observer) {
    entries.forEach(entry => {
        if(!entry.isIntersecting) {
            headerElement.classList.add('scrolled');
        } else {
            headerElement.classList.remove('scrolled');
        }
    });
}, options);

observer.observe(breakingPointElement);
